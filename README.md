# Organising 4 All

# Table of Contents

1. [Educational](#Educational)
2. [Resources](#Resources)
3. [How-To](#How-To)
    1. [GENERAL](#General)
    2. [MUTUAL-AID](#Mutual-Aid)
    3. [HOUSING](#Housing)
    4. [WORKPLACE](#Workplace)
    5. [COMMUNITY](#Community)
    6. [SCHOOL](#School)
    7. [DIRECT-ACTION](#Direct-Action)
    8. [IMMIGRATION](#Immigration)
    9. [MEDIA](#Media)
    10. [AGITPROP](#Agitprop)
4. [Security-Cultures](#Security-Cultures)
5. [Others](#Others)

## Educational <a name="Educational"></a>

- [Rebel Steps](https://rebelsteps.com/)

- [Mutual Aid: Solidarity Not Charity](http://www.deanspade.net/wp-content/uploads/2020/03/Mutual-Aid-Article-Social-Text-Final.pdf)

- [What Mutual Aid Can Do During a Pandemic](https://www.newyorker.com/magazine/2020/05/18/what-mutual-aid-can-do-during-a-pandemic)

- [In the Navajo Nation, Anarchism Has Indigenous Roots](https://archive.is/jIsyu) (article about Covid-19 Mutual Aid in the Navajo Nation)

- [5 steps To Start Mutual Aid! | Building An Alternative To Capitalism](https://www.youtube.com/watch?v=nQooQVixTPo)

- [Autonomous Mutual Aid Groups Mobilize in Texas as Death Toll Rises](https://archive.is/d7GaI)

- [Black Autonomy Podcast: Dual Power: A Simple Explanation](https://blackautonomy.libsyn.com/dual-power-a-simple-explanation)

- [Mutual Aid And Dual Power | Praxis 101](https://www.youtube.com/watch?v=4HESYrW-0eg)

- [La Morada Mutual Aid Soup Kitchen](https://www.youtube.com/watch?v=NLEZo1tlTgo)

- [How Mutual Aid Supported Chinatown When the Government Failed](https://outline.com/2HwJZ4)

- [Mutual Aid: A Factor Of Evolution](https://theanarchistlibrary.org/library/petr-kropotkin-mutual-aid-a-factor-of-evolution)

- [Kropotkin Was No Crackpot](https://theanarchistlibrary.org/library/stephen-jay-gould-kropotkin-was-no-crackpot)

- [More Than Facebook: Mutual Aid Organizing in Ottawa During COVID-19](https://outline.com/Etw8X2)

- [Activists Are Sharing Land In Vermont With People Escaping Climate Disaster](https://www.vice.com/en/article/wx5zb9/activists-are-buying-land-in-vermont-to-help-people-escape-climate-disaster)

## Resources <a name="Resources"></a>

- [COVID-19 Mutual Aid - It's Going Down](https://itsgoingdown.org/c19-mutual-aid/) / [MIRROR](https://archive.is/G9zM9)

- [2021 Food Not Bombs Locations](https://www.google.com/maps/d/viewer?mid=1KVbOaPBP2Xh1zk59DS9nI-BjjYnrwtwD)

- [Mutual Aid Wiki](https://mutualaid.wiki/map)

- [Mutual Aid NYC](https://mutualaid.nyc/mutual-aid-groups/)

- [Twin Cities Mutual Aid Map](https://twin-cities-mutual-aid.org/)

- [Reach4Help](https://map.reach4help.org/)

- [Mutual Aid Disaster Relief](https://mutualaiddisasterrelief.org/)

- [A Center for Grassroots Organizing](https://www.grassrootscenter.net/)

- [Common Ground Relief](https://www.commongroundrelief.org/)

- [Youth Liberation Front](https://youthliberation.noblogs.org/)

- [IWW Directory](https://iww.org/directory/)

- [Anarchist Black Cross Federation](https://www.abcf.net/contact/)

- [SRA Local Chapters](https://socialistra.org/chapters/)

- [Puget Sound John Brown Gun Club](https://psjbgc.org/)

- [Huey P Newton Gun Club](https://www.hueypnewtongunclub.org/)

- [FIREFUND - Organized Solidarity](https://www.firefund.net/)

- [Radical Servers](https://riseup.net/en/security/resources/radical-servers)

## How-To <a name="How-To"></a>

### General <a name="General"></a>

- [Basics of Organizing](https://theanarchistlibrary.org/library/central-nj-iww-basics-of-organizing)

- [An Anarchist Organising Manual](https://theanarchistlibrary.org/library/zabalaza-an-anarchist-organising-manual)

- [Building: A DIY Guide to Creating Spaces, Hosting Events and Fostering Radical Communities](https://www.sproutdistro.com/catalog/zines/organizing/building-diy-guide-creating-spaces-hosting-events-fostering-radical-communities)

- [Sprout Distro's Organizing Zines](https://www.sproutdistro.com/catalog/zines/organizing/)

- [Sprout Distro's Direct Action Zines](https://www.sproutdistro.com/catalog/zines/direct-action/)

- [Sprout Distro's DIY Zines](https://www.sproutdistro.com/catalog/zines/diy/)

- [Sprout Distro's Security Zines](https://www.sproutdistro.com/catalog/zines/security/)

- [Libcom Organise](https://libcom.org/organise)

- [Recipes for Disaster: an anarchist cookbook](https://archive.org/details/RecipesForDisasterAnAnarchistCookbook)

- [Earth First! Direct Action Manual](https://archive.org/details/direct_action_manual_3)

- [Personal guides](https://libcom.org/organise/personal-guides)

- [How to start a group](https://libcom.org/organise/general/articles/how-to-start-a-group.php)

- [Basic principles of revolutionary organisation](https://libcom.org/library/basic-principles-revolutionary-organisation)

- [Coming up with a strategy and set of principles](https://libcom.org/organise/general/articles/coming-up-with-a-strategy-principles.php)

- [Decision making and organisational form](https://libcom.org/organise/general/articles/decision-making-and-organisational-form.php)

- [Financing a group](https://libcom.org/organise/general/articles/financing-a-group.php)

- [Getting the Word Out: Guide to Promoting Events](https://libcom.org/library/getting-word-out-guide-promoting-events)

- [Handling difficult behaviour in meetings](https://libcom.org/organise/general/articles/handling-difficult-behaviour-in-meetings.php)

- [How to organise and facilitate meetings effectively](https://libcom.org/organise/general/articles/how-to-organise-and-facilitate-meetings-effectively.php)

- [Successful delegation guide](https://libcom.org/organise/general/articles/tips-for-delegation.php)

- [Taking meeting minutes guide](https://libcom.org/organise/taking-meeting-minutes)

- [Winning Phone Zap Campaigns: An Interview with Oakland IWOC](https://libcom.org/library/winning-phone-zap-campaigns-interview-oakland-iwoc)

- [Building a solidarity network guide](https://libcom.org/library/you-say-you-want-build-solidarity-network)

- [Federations and networks guide](https://libcom.org/organise/general/articles/federations-networks.php)

### Mutual-Aid <a name="Mutual-Aid"></a>

- [Building Mutual Support and Organising in Our Communities](https://libcom.org/library/building-mutual-support-organising-our-communities)

- [How to Start a Mutual Aid Group](https://www.youtube.com/watch?v=lDmJZpmh9vI)

- [SEVEN STEPS TO STARTING A FOOD NOT BOMBS GROUP](http://www.foodnotbombs.net/seven.html)

- [The Definitive Guide to Dumpster Diving](https://medium.com/@jamieklinger/the-definitive-guide-to-dumpster-diving-41ca27168ba5)

- [The Debt Resisters’ Operations Manual](https://strikedebt.org/drom/) / [PDF](https://radicaldefence.keybase.pub/DIRECT-ACTION/TACTICS/The%20Debt%20Resisters%20Operations%20Manual%20by%20Strike%20Debt.pdf)

- [Trouble #24 - Organize: For Autonomy & Mutual Aid](https://www.youtube.com/watch?v=Z2Gzh7m4sSk)

- [Steal This Book](https://theanarchistlibrary.org/library/abbie-hoffman-steal-this-book)

- [Retail Theft Wiki](https://raddle.me/wiki/shoplifting)

- [How to Find and Use Blind Spots](https://raddle.me/wiki/blindspots)

- [Shoplifting Guide Quadrilogy](https://raddle.me/wiki/liftguides)

- [The Shoplifting Master List](https://raddle.me/w/shopliftingml)

- [List of Source Tagged Items](https://raddle.me/w/shopliftingsourcetags)

- [A Shoplifter's Guide to Tags](https://raddle.me/w/ShopliftingTags)

- [A Shoplifter's Guide to Magnets](https://raddle.me/w/ShopliftingMagnets)

- [A Socialist Thanks-Taking 🍽️](https://www.youtube.com/watch?v=IytagqWdZgA)

- [Quick tip: Self Checkout](https://www.youtube.com/watch?v=OD5KsqiKP7A)

- [@theroamingchef mutual-aid video](https://www.tiktok.com/@theroamingchef/video/6972392336441511169?enable_clips=1&language=en&share_app_id=1233&share_item_id=6972392336441511169&share_link_id=39972037-0CE6-4089-982E-760B54359220&tt_from=sms&_r=1&is_copy_url=1&is_from_webapp=v1)

- [Heat Mutual Aid](https://heatmutualaid.github.io/)

### Housing <a name="Housing"></a>

- [Why and How to Go on a Rent Strike](https://itsgoingdown.org/why-and-how-to-go-on-a-rent-strike/) / [MIRROR](https://archive.vn/ZKKXw)

- [Beyond Squat or Rot: Anarchist Approaches to Housing](https://www.sproutdistro.com/catalog/zines/organizing/beyond-squat-or-rot)

- [Do-It-Yourself Occupation Guide: 2012 Redux](https://www.sproutdistro.com/catalog/zines/direct-action/do-it-yourself-occupation-guide)

- [Tenant Organizing Manual](http://www.tenant.net/Organize/Lenox/lh-1.html)

- [Tools for Building Tenant Power: Tactics Vol #1](https://libcom.org/library/tools-building-tenant-power-tactics-vol-1)

- [Eviction Defense Handbook](https://libcom.org/library/eviction-defense-handbook)

- [Setting up housing co-operatives guide](https://libcom.org/organise/housing/articles/housing-co-op-setting-up-guide.php)

- [Squatters' Handbook](https://donostialdeaokupatu.files.wordpress.com/2013/05/squatter-s-handbook-england-13th-edition.pdf)

- [It’s Vacant, Take It!](https://www.sproutdistro.com/catalog/zines/direct-action/its-vacant-take-it)

- [Squatting guide](https://libcom.org/organise/housing/articles/squatting-guide.php)

- [Section 6 legal notice for squats](https://libcom.org/organise/housing/articles/section-6-notice.php)

- [How to Throw a Squatted Dance Party](https://theanarchistlibrary.org/library/crimethinc-how-to-throw-a-squatted-dance-party)

- [Squatters' Handbook: Political Squatting Tips](https://www.sproutdistro.com/catalog/zines/organizing/squatters-handbook)

### Workplace <a name="Workplace"></a>

- [IWW Organizing Manual](https://theanarchistlibrary.org/library/industrial-workers-of-the-world-iww-organizing-manual)

- [How to Organize Your Workplace Without Getting Caught](https://www.vice.com/en/article/y3md3v/how-to-organize-your-workplace-without-getting-caught)

- [You say you want a general strike](https://organizing.work/2019/08/you-say-you-want-a-general-strike/) / [A blueprint for a general strike in our time](https://organizing.work/2019/10/a-blueprint-for-a-general-strike-in-our-time/)

- [No More Fake Strikes](https://organizing.work/2019/08/no-more-fake-strikes/) / [Big Strikes and the sabotage of the labor movement](https://organizing.work/2020/11/big-strikes-and-the-sabotage-of-the-labor-movement/)

- [Organising at work: introduction](https://libcom.org/organise/organising-at-work-the-basics)

- [Organising your workplace: getting started](https://libcom.org/organise/workplace/articles/organising-at-work-guide)

- [Organising at work: some basic principles](https://libcom.org/organise/workplace/articles/basic-principles.php)

- [Dealing with bullying at work guide](https://libcom.org/organise/dealing-with-bullying-at-work-guide)

- [Guide to taking strike action](https://libcom.org/organise/workplace/articles/taking-strike-action.php)

- [Wildcat or official strike action?](https://libcom.org/organise/workplace/articles/why-wildcat.php)

- [Dual power at work](https://libcom.org/organise/workplace/articles/dual-power.php)

- [Go-slow guide](https://libcom.org/organise/workplace/articles/go-slow.php)

- [Good work strike](https://libcom.org/organise/workplace/articles/good-work-strike.php)

- [Guide to sick-outs](https://libcom.org/organise/workplace/articles/sick-in.php)

- [Selective strikes](https://libcom.org/organise/workplace/articles/selective-strikes.php)

- [Sitdown strike or occupation guide](https://libcom.org/organise/workplace/articles/sitdown-strike.php)

- [Whistle-blowing guide](https://libcom.org/organise/workplace/articles/whistle-blowing.php)

- [Work-to-rule: a guide](https://libcom.org/organise/workplace/articles/work-to-rule.php)

- [Making the most of spontaneous rebellions at work](https://libcom.org/organise/workplace/articles/spontaneous-rebellions-at-work.php)

- [Sabotage in the workplace](https://libcom.org/organise/workplace/articles/sabotage.php)

- [Solidarity against sexism on the shop floor](https://libcom.org/library/solidarity-against-sexism-shop-floor)

- [How To Justify Workplace Theft](https://theanarchistlibrary.org/library/crimethinc-how-to-justify-workplace-theft)

### Community <a name="Community"></a>

- [Build Your Own Solidarity Network](https://theanarchistlibrary.org/library/seattle-solidarity-network-build-your-own-solidarity-network)

- [Community Control of the Poor Community](https://theanarchistlibrary.org/library/lorenzo-kom-boa-ervin-community-control-of-the-poor-community)

- [Starting an Anarchist Black Cross – A Guide](https://itsgoingdown.org/starting-an-anarchist-black-cross-a-guide/) / [MIRROR](https://archive.ph/tXPgG)

- [Accountability, Backbone, and Change: Protocol for Community Care in Instances of Abuse](https://itsgoingdown.org/accountability-backbone-and-change-protocol-for-community-care-in-instances-of-abuse/) / [MIRROR](https://archive.vn/riI34)

- [Restorative Justice: A Path Forward](https://itsgoingdown.org/restorative-justice-a-path-forward/) / [MIRROR](https://archive.ph/wEBqM)

- [Door knocking guide](https://libcom.org/organise/community/articles/door-knocking-tips.php)

- [Get Up and Get Going: How to Form a Group](https://libcom.org/library/get-get-going-how-form-group)

- [How To Set Up An Anti-Raids Group](https://libcom.org/library/how-set-anti-raids-group)

- [How to start a community kitchen](https://libcom.org/library/how-start-community-kitchen)

- [Key ideas for community organising](https://libcom.org/organise/key-ideas-for-community-organising)

- [Organising in our Communities: Housing Action Southwark and Lambeth](https://libcom.org/library/organising-our-communities-housing-action-southwark-lambeth)

- [Resources for starting an antifascist group](https://libcom.org/library/resources-starting-antifascist-group)

- [Things you can do to build resistance to raids and the hostile environment](https://libcom.org/library/things-you-can-do-build-resistance-raids-hostile-environment)

### School <a name="School"></a>

- [Student Organiser's Handbook](http://studenthandbook.ourproject.org/book/intro.html)

- [University Activist Handbook](https://www.dropbox.com/sh/zx7bkdb4facbzy4/AADuJTpyFJkwVI3e2HP1e7kca/University%20Activist%20Handbook.pdf?dl=0)

### Direct-Action <a name="Direct-Action"></a>

- [Affinity Groups](https://theanarchistlibrary.org/library/destructables-affinity-groups)

- [How to Organize a Protest March](https://www.sproutdistro.com/catalog/zines/direct-action/how-to-organize-a-protest-march)

- [A Civilian’s Guide to Direct Action](https://theanarchistlibrary.org/library/crimethinc-a-civilian-s-guide-to-direct-action)

- [How to Survive Anti-Police Protests](https://theanarchistlibrary.org/library/shane-burley-how-to-survive-anti-police-protests)

- [Lasers in the Tear Gas: A Guide to Tactics in Hong Kong](https://itsgoingdown.org/lasers-in-the-tear-gas/) / [MIRROR](https://archive.ph/xiaAr)

- [Radical Defense](https://www.sproutdistro.com/catalog/zines/direct-action/radical-defense)

- [Elements of A Barricade](https://www.sproutdistro.com/catalog/zines/direct-action/elements-of-a-barricade)

- [Excited Delirium](https://www.sproutdistro.com/catalog/zines/direct-action/excited-delirium)

- [Don't Back Down!](https://www.sproutdistro.com/catalog/zines/direct-action/dont-back-down)

- [Direct Action Tactics](https://www.sproutdistro.com/catalog/zines/direct-action/direct-action-tactics/)

- [Of Martial Traditions & The Art of Rebellion](https://www.sproutdistro.com/catalog/zines/direct-action/of-martial-traditions)

- [10 Steps for Setting Up A Blockade](https://www.sproutdistro.com/catalog/zines/direct-action/10-steps-blockade)

- [The Black Flag Catalyst Revolt Guide](https://theanarchistlibrary.org/library/bfc-revolt-guide)

- [Mask Up: How & Why](https://www.sproutdistro.com/catalog/zines/direct-action/mask-up)

- [A Field Guide to Protests: The Protest Marshall](https://itsgoingdown.org/field-guide-protests-protest-marshall/)

- [How to win at kettling](https://theanarchistlibrary.org/library/anarch-ish-how-to-win-at-kettling-a-guide-for-non-policemen)

- [How To Break The Kettle: An Illustrated Scientific Guide](https://bcwthebristolhum.blogspot.com/p/how-to-break-kettle.html)

- [Warrior Crowd Control & Riot Manual](https://www.sproutdistro.com/catalog/zines/direct-action/warrior-crowd-control-riot-manual)

- [(A)BC's Mini Guide to Protesting](https://www.sproutdistro.com/catalog/zines/direct-action/abcs-mini-guide-protesting)

- [Riot Medicine](https://riotmedicine.net/)

- [Health and Safety at Militant Actions](https://www.sproutdistro.com/catalog/zines/direct-action/health-and-safety-at-militant-actions)

- [A Demonstrator's Guide to Responding to Gunshot Wounds](https://www.sproutdistro.com/catalog/zines/direct-action/responding-gunshot-wounds)

- [A Demonstrator’s Guide to Understanding Police Batons](https://crimethinc.com/2020/12/15/a-demonstrators-guide-to-understanding-police-batons-and-how-to-protect-against-them)

- [A Demonstrator’s Guide to Gas Masks and Goggles](https://crimethinc.com/2020/09/02/a-demonstrators-guide-to-gas-masks-and-goggles-everything-you-need-to-know-to-protect-your-eyes-and-lungs-from-gas-and-projectiles)

- [A Demonstrator’s Guide to Body Armor](https://crimethinc.com/2020/12/15/a-demonstrators-guide-to-body-armor-protecting-yourself-against-blows-batons-bullets-and-more)

- [A Demonstrator’s Guide to Helmets](https://crimethinc.com/2020/09/01/a-demonstrators-guide-to-helmets-everything-you-need-to-know)

- [An Activist's Guide to Basic First Aid](https://www.sproutdistro.com/catalog/zines/direct-action/activists-guide-to-basic-first-aid)

- [Banner Drops, Stencils, Wheatpaste, and Distributing Information](https://www.sproutdistro.com/catalog/zines/direct-action/banner-drops-stencils-wheatpaste)

- [How to Topple a Statue Using Science](https://outline.com/SZaGkP)

- [Basic Blockading](https://www.sproutdistro.com/catalog/zines/direct-action/basic-blockading)

- [Basic Recon Skills](https://www.sproutdistro.com/catalog/zines/direct-action/basic-recon-skills)

- [Bodyhammer: Tactics and Self-Defense for the Modern Protestor](https://www.sproutdistro.com/catalog/zines/direct-action/bodyhammer)

- [Defend the Territory](https://www.sproutdistro.com/catalog/zines/direct-action/defend-the-territory)

- [Direct Action Survival Guide](https://www.sproutdistro.com/catalog/zines/direct-action/direct-action-survival-guide)

- [Pepperspray, CS, & Other "Less Lethal" Weapons](https://www.sproutdistro.com/catalog/zines/direct-action/pepperspray-cs-less-lethal-weapons)

- [Random Street Medic - Mask myths](https://www.youtube.com/watch?v=wUg0OK5kwBs)

- [Principles & Practices for Marshaling A Protest. A Primer.](https://twincitiesgdc.org/principles-practices-for-marshaling-a-protest-a-primer/)

- [Resistance in the Street - A Guide to Keeping Safe & Free in Crowd Control Situations](https://www.sproutdistro.com/catalog/zines/direct-action/resistance-in-the-street)

- [Copse: A Cartoon Book of Tree Protesting](https://www.sproutdistro.com/catalog/zines/direct-action/copse-a-cartoon-book-of-tree-protesting)

- [How It Is To Be Fun](https://www.sproutdistro.com/catalog/zines/direct-action/how-it-is-to-be-fun)

### Immigration <a name="Immigration"></a>

- [Crossing the United States Border: A Security Guide for Citizens and Non-Citizens](https://itsgoingdown.org/crossing-the-united-states-border-a-security-guide-for-citizens-and-non-citizens/) / [MIRROR](https://archive.vn/yWhJQ)

### Media <a name="Media"></a>

- [Talking to the Media: Avoiding the Pitfalls: A Guide for Anarchists](https://theanarchistlibrary.org/library/crimethinc-talking-to-the-media)

- [How to Write a Press Release](https://libcom.org/library/how-write-press-release)

- [Independent media guide](https://libcom.org/organise/media)

- [Guide to dealing with the corporate media](https://libcom.org/organise/guide-to-dealing-with-the-corporate-media)

### Agitprop <a name="Agitprop"></a>

- [Hell Yeah Anarchist Posters](https://hellyeahanarchistposters.tumblr.com)

- [Rechelon's Zine Library](https://github.com/rechelon/zine_library)

- [Sprout Distro's Zine List](https://www.sproutdistro.com/catalog/zines/)

- [Socialist Screen Printing (For cheap)](https://www.youtube.com/watch?v=mE5ox6Uytfg)

- [McDonalds $15 Strike Agitation](https://www.youtube.com/watch?v=79jIYXPSlB0)

- [RedDocs™: Freedom of information](https://www.youtube.com/watch?v=LxIr0bGMZhI)

## Security-Cultures <a name="Security-Cultures"></a>

- [Confidence Courage Connection Trust: A proposal for security culture](https://north-shore.info/2019/11/05/confidence-courage-connection-trust-a-proposal-for-security-culture/)

- [Doxcare](https://theanarchistlibrary.org/library/crimethinc-doxcare)

- [Take Care of Yourself, Take Care of Your Friends: Some Starting Places for Drawing Digital Boundaries](https://itsgoingdown.org/take-care-take-care-friends-starting-places-drawing-digital-boundaries/) / [MIRROR](https://archive.ph/3AyUZ)

- [In Defense of Smashing Cameras](https://www.sproutdistro.com/catalog/zines/direct-action/defense-smashing-cameras)

- [The Invisible Ground](https://www.sproutdistro.com/catalog/zines/security/invisible-ground)

- [Security Culture — Anti-Speciesist Action](https://antispeciesistaction.com/security-culture)

- [Primer on how to buy shit and have it not end up in a federal indictment someday](https://twitter.com/EmilyGorcenski/status/1268627830868451330)

- [Emily Gorcenski's Burner Guide](https://twitter.com/EmilyGorcenski/status/1381741078123061248) (best burner guide on the internet)

- [B3RN3D's Burner Phone Best Practices](https://b3rn3d.herokuapp.com/blog/2014/01/22/burner-phone-best-practices/)

- [CrimethInc. : Burner Phone Best Practices](https://crimethinc.com/2017/03/27/burner-phone-best-practices)

- [How to Prepare a Cheap Burner Phone for Protesting](https://www.youtube.com/watch?v=r4gqRYkA9KE)

- [Defending Space: Security for Buildings and Physical Spaces](https://itsgoingdown.org/defending-space-security-for-buildings-and-physical-spaces/) / [MIRROR](https://archive.vn/Mzvvw)

- [Signal Fails](https://north-shore.info/2019/06/02/signal-fails/)

- [EFF Surveillance Self Defense](https://ssd.eff.org/) / [Attending a Protest](https://ssd.eff.org/en/module/attending-protest)

- [Security Planner](https://securityplanner.consumerreports.org/)

- [PrivacyTools](https://www.privacytools.io/)

## Others <a name="Others"></a>

- [The /f/Piracy wiki](https://raddle.me/wiki/piracy)

- [Radical Defence](https://radicaldefence.keybase.pub)

